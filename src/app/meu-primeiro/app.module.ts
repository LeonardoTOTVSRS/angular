import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { MeuPrimeiroComponent } from "./meu-primeiro.component";
import { MeuPrimeiro2Component } from "../meu-primeiro2/meu-primeiro2.component";
import { CursosModule } from "../cursos/cursos.module";
import { CursoDetalheComponent } from "../cursos/curso-detalhe/curso-detalhe.component";

@NgModule({
  declarations: [AppComponent, MeuPrimeiroComponent, MeuPrimeiro2Component],
  imports: [BrowserModule, CursosModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
